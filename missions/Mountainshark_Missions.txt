g_mountainshark_column_4 = {
	slot = 4
	generic = no
	ai = yes
	potential = {
		tag = H84
	}
	has_country_shield = yes

    g_an_unexpected_journey = {
		icon = mission_fortify_rajputana
		position = 1
		required_missions = {

		}
		trigger = {
			adm_power = 100
		}
		
		effect = {
			add_adm_power = -100
			add_country_modifier = {
				name = cave_goblin_hobbit_journey
				duration = 9125
			}
		}
	}
	g_the_plans_of_tolzheen = {
		icon = mission_fortify_rajputana
		position = 2
		required_missions = {
			g_an_unexpected_journey
		}
		trigger = {
			OR = {
				theologian = 2
				philosopher = 2
			}
			mil_power = 150
		}
		
		effect = {
			add_mil_power = -150
			define_general = {
				shock = 8
				fire = 6
				manuever = 0
				siege = 0
				name = "Jararar Tolzheen"
				trait = war_wizard_personality
			}
		}
	}
	g_the_battle_of_five_armies = {
		icon = mission_fortify_rajputana
		position = 3
		required_missions = {
			g_the_plans_of_tolzheen
		}
		trigger = {
			any_core_province = {
				culture_group = orc
			}
			any_core_province = {
				culture_group = goblin
			}
			any_core_province = {
				culture_group = dwarven
			}
			any_core_province = {
				culture_group = ogre
			}
			any_core_province = {
				culture_group = kobold
			}
		}
		
		effect = {
			add_prestige = 100
			add_splendor = 100
			add_treasury = 100
		}
	}
	g_the_reforms_of_pether_jakzon = {
		icon = mission_fortify_rajputana
		position = 4
		required_missions = {
			g_the_battle_of_five_armies
		}
		trigger = {	
			artist = 4
		}
		
		effect = {
			add_country_modifier = {
				name = cave_goblin_peter_jackson
				duration = 9125
			}
			define_general = {
				shock = 2
				fire = 2
				manuever = 8
				siege = 1
				name = "Pether Jakzon"
				trait = glory_seeker_personality
			}
		}
	}
	g_goblins_of_the_mithril_mountains = {
		icon = mission_fortify_rajputana
		position = 5
		required_missions = {
			g_the_reforms_of_pether_jakzon
		}
		trigger = {
			calc_true_if = {
				any_known_country = {
					truce_with = FROM
				}
				amount = 6
			}
		}
		
		effect = {
			add_country_modifier = {
				name = cave_goblin_mountainshark
				duration = -1
			}
			#swap_non_generic_missions = yes
			swap_free_idea_group = yes
			override_country_name = DIVLKLAN # Free Land
		}
	}
}

#g_mountainshark_lotr_column_4 = {
#	slot = 4
#	generic = no
#	ai = yes
#	potential = {
#		tag = H84
#		has_country_modifier = cave_goblin_mountainshark
#	}
#	has_country_shield = yes
#
#	g_the_lord_of_the_guns = {
#		icon = mission_fortify_rajputana
#		position = 1
#		required_missions = {
#
#		}
#		trigger = {
#	
#		}
#		
#		effect = {
#
#		}
#	}
#	g_gazdalk_the_gray = {
#		icon = mission_fortify_rajputana
#		position = 1
#		required_missions = {
#
#		}
#		trigger = {
#	
#		}
#		
#		effect = {
#
#		}
#	}
#	g_the_fellowship_of_the_gun = {
#		icon = mission_fortify_rajputana
#		position = 1
#		required_missions = {
#
#		}
#		trigger = {
#	
#		}
#		
#		effect = {
#
#		}
#	}
#	g_the_twin_towers = { #never forget
#		icon = mission_fortify_rajputana
#		position = 1
#		required_missions = {
#
#		}
#		trigger = {
#			#wreck a world trade center or have one
#		}
#		
#		effect = {
#
#		}
#	}
#	g_the_return_of_the_clanboss = {
#		icon = mission_fortify_rajputana
#		position = 1
#		required_missions = {
#
#		}
#		trigger = {
#	
#		}
#		
#		effect = {
#
#		}
#	}
#    g_the_new_shadow = {
#		icon = mission_fortify_rajputana
#		position = 1
#		required_missions = {
#
#		}
#		trigger = {
#	
#		}
#		
#		effect = {
#
#		}
#	}
#}

#these trees show up during the allclan civil war. Replaces generic resurce missions and special warband ones
g_mountainshark_civil_war_region_column_4 = { #involves doing stuff with their spawn area
	slot = 4
	generic = no
	ai = yes
	potential = {
		tag = H84
		has_country_modifier = allclan_split
	}
	has_country_shield = yes

    g_establishing_our_clan = {
		icon = mission_fortify_rajputana
		position = 1
		required_missions = {

		}
		trigger = {
	
		}
		
		effect = {
			
		}
	}
}
g_mountainshark_civil_war_column_5 = {
	slot = 5
	generic = no
	ai = yes
	potential = {
		tag = H84
		has_country_modifier = allclan_split
	}
	has_country_shield = yes

    g_the_great_kinstrife = {
		icon = mission_fortify_rajputana
		position = 1
		required_missions = {

		}
		trigger = {
			OR = {
				has_country_modifier = allclan_split
			}	
		}
		
		effect = {
			create_general = {
				tradition = 100
				#add_siege = -1
			}
			override_country_name = DIVLKLAN # Savage Clan
			swap_free_idea_group = yes
		}
	}
}