#Country Name: Please see filename.

graphical_culture = westerngfx

color = { 37 128 21 }

revolutionary_colors = { 37 128 21 }

historical_idea_groups = {
	aristocracy_ideas
	offensive_ideas
	expansion_ideas
	quantity_ideas	
	administrative_ideas	
	trade_ideas
	quality_ideas
	innovativeness_ideas
}

historical_units = {
	western_medieval_infantry
	chevauchee
	western_men_at_arms
	swiss_landsknechten
	dutch_maurician
	french_caracolle
	anglofrench_line
	french_dragoon
	french_bluecoat
	french_cuirassier
	french_impulse
	mixed_order_infantry
	open_order_cavalry
	napoleonic_square
	napoleonic_lancers
}

ship_names = {
	M�itsa Kuskelaiba Taeva Tordi Palta Leripungi Enna Vuur Sare Sarestakod Lagsa Taseng Tasmua Lagsasiiv Leeho Metsulik Nitt Seani
	Veestous Peengiiv Vahjevlaiba Vees Mua Ilim Kun Loiva Vuusdlaiba Puulkatis Bun Sine Vher� Kelte Ruusk Valak Muuts Harama Vatsalta
	Lattama Surgiiv Kuld Meer Katjurvaik Kaagovees Varjlur Vaskari Villakane Ensmitekpua Suruppu Taukkeslaiba Survalral Kasvaata Parjuma
	Klasen Kirselev Mearuunek V��tual Pont�hsaarn E�nt�hsaarn L�nt�hsaarn T�nt�hsaarn S�tsare Tuukada Kujoda Rahlse Taikaan Jurlann
}

army_names = {
	"Iron Wolves"
}

fleet_names = {
	Seawolves
}