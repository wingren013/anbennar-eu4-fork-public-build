#Country Name: Please see filename.

graphical_culture = westerngfx

color = { 78 173 71 }

revolutionary_colors = { 107 208 76 }

historical_idea_groups = {
	aristocracy_ideas
	offensive_ideas
	expansion_ideas
	quantity_ideas	
	administrative_ideas	
	trade_ideas
	quality_ideas
	innovativeness_ideas
}

historical_units = {
	western_medieval_infantry
	chevauchee
	western_men_at_arms
	swiss_landsknechten
	dutch_maurician
	french_caracolle
	anglofrench_line
	french_dragoon
	french_bluecoat
	french_cuirassier
	french_impulse
	mixed_order_infantry
	open_order_cavalry
	napoleonic_square
	napoleonic_lancers
}

monarch_names = {
	Khan = 1
}

leader_names = {
	Khan Super Doom
}

ship_names = {
	Khan Super Doom
}

army_names = {
	Khan Super Doom
}

fleet_names = {
	Khan Super Doom
}